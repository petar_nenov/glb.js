export * from './components/App';
export * from './components/Book';
export * from './components/Customer';
export * from './components/Library';
export * from './components/Paper';

// import GreatLibrary from './components/App';
// import Book from './components/Book';
// import Customer from './components/Customer';

// /* book */
// const book: Book = new Book();
// book.author = {
//   birdDate: '01/01/2001',
//   name: 'Jack Mack',
//   nationality: 'Chine',
// };
// book.pages = 234;
// book.title = 'Best way to blow your mind';
// book.rentPeriod = 14;

// book.rentedDate = new Date().toUTCString();
// book.returnDate = new Date(
//   new Date().setDate(new Date().getDate() + book.rentPeriod),
// ).toUTCString();

// /* customer */
// const customer = new Customer();
// customer.name = 'Purple Jam';
// customer.rentedBooks.push(book as Required<Book>);
// customer.rentedBooks[0].vote('up');

// /* Great Library */
// GreatLibrary.books.push(book);
// GreatLibrary.customers.push(customer);
// GreatLibrary.address = {
//   country: 'USA',
//   street: {
//     name: '5th Avenue',
//     number: 22,
//   },
//   town: 'New York',
// };
// GreatLibrary.title = 'GreatLibrary';

// const div = document.querySelector('#app') as HTMLDivElement;
// div.innerHTML = `<pre>${JSON.stringify(GreatLibrary, null, 2)}</pre>`;
