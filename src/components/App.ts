import { Library } from './Library';

class App {
  library: Library = new Library();
}

export const GreatLibrary = new App().library;
