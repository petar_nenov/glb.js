class Paper {
  material: string = '';
  burn() {
    return 'Paper burning...';
  }
}

interface Paper {
  material: string;
  burn(): string;
}

export { Paper };
