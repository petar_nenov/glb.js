import { Book } from './Book';

export class Customer {
  name: string = 'N/A';
  rentedBooks: Required<Book>[] = [];
}
