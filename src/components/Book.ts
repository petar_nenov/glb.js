import { Paper } from './Paper';

class Book extends Paper {
  title: string = 'N/A';
  author: Author = {} as Author;
  pages: number = NaN;
  rentPeriod: number = NaN;
  popularity?: number = NaN;
  rentedDate?: string = 'N/A';
  returnDate?: string = 'N/A';
  vote(thumb: Thumb) {
    const val = thumb === 'up' ? 1 : -1;
    this.popularity = this.popularity ? this.popularity + val : val;
  }
}

interface Book extends Paper {
  title: string;
  author: Author;
  pages: number;
  rentPeriod: number;
  popularity?: number;
  rentedDate?: string;
  returnDate?: string;
  vote(thumb: Thumb): void;
}

export { Book };
export interface Author {
  name: string;
  birdDate: string;
  nationality: string;
}
export type Thumb = 'up' | 'down';
