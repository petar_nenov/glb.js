import { Book } from './Book';
import { Customer } from './Customer';

export class Library {
  title: string = 'N/A';
  address: Address = {} as Address;
  customers: Customer[] = [];
  books: Book[] = [];
}

export interface Address {
  country: string;
  town: string;
  street: {
    name: string;
    number: number;
  };
}
